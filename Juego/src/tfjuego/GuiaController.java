/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package tfjuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Alguien
 */
public class GuiaController implements Initializable {

    @FXML
    private Button control;
    
    @FXML
    private Button btnVolver;

    @FXML
    private ImageView imgGuia;
    
    private int posicion = 1;
    
    Media mMover = new Media(new File("src/sfx/menuNav.wav").toURI().toString());
    AudioClip mover = new AudioClip(mMover.getSource());
    
    Media mAtras = new Media(new File("src/sfx/atras.wav").toURI().toString());
    AudioClip atras = new AudioClip(mAtras.getSource());

    @FXML
    void control(KeyEvent event) {
        if (event.getCode().equals(KeyCode.RIGHT)) {
            mover.play();
            switch (posicion) {
                case 1:
                    imgGuia.setImage(new Image("imagenes/misc/guia/2.png"));
                    posicion = 2;
                    break;
                case 2:
                    imgGuia.setImage(new Image("imagenes/misc/guia/3.png"));
                    posicion = 3;
                    break;
                case 3:
                    imgGuia.setImage(new Image("imagenes/misc/guia/4.png"));
                    posicion = 4;
                    break;
                case 4:
                    imgGuia.setImage(new Image("imagenes/misc/guia/5.png"));
                    posicion = 5;
                    break;
                default:
                    imgGuia.setImage(new Image("imagenes/misc/guia/1.png"));
                    posicion = 1;
                    break;
            }
        } else if (event.getCode().equals(KeyCode.LEFT)) {
            mover.play();
            switch (posicion) {
                case 5:
                    imgGuia.setImage(new Image("imagenes/misc/guia/4.png"));
                    posicion = 4;
                    break;
                case 4:
                    imgGuia.setImage(new Image("imagenes/misc/guia/3.png"));
                    posicion = 3;
                    break;
                case 3:
                    imgGuia.setImage(new Image("imagenes/misc/guia/2.png"));
                    posicion = 2;
                    break;
                case 2:
                    imgGuia.setImage(new Image("imagenes/misc/guia/1.png"));
                    posicion = 1;
                    break;
                default:
                    imgGuia.setImage(new Image("imagenes/misc/guia/5.png"));
                    posicion = 5;
                    break;
            }
        } else if (event.getCode().equals(KeyCode.Z) || (event.getCode().equals(KeyCode.X))) {
            btnVolver.fire();
            atras.play();
        }
    }
    
    @FXML
    void volver(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnVolver.setVisible(false);
        mover.setVolume(0.50);
        atras.setVolume(0.50);
    }    
    
}
