/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package tfjuego;

import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Alguien
 */
public class CargaController implements Initializable {

    @FXML
    private Button btnCambio;
    
    @FXML
    private Label lblConsejo;
    
    private String[] mensajes = new String[]{
        //"Texto muy largo de placeholder jajajajajajajajajajajajajajajajajajajajaja",
        //consejos normales
        "Si recibes un golpe, puede que quedes aturdido",
        "Usa objetos para recuperar puntos de vida",
        "Por favor, lee la guía al menos una vez, en serio",
        "El mareo se pasa durante la mitad del tercer turno",
        //aqui empieza lo divertido
        "Originalmente este juego iba a tener cuatro personajes",
        "Esta pantalla en realidad no sirve de nada",
        "Bebo para olvidar el código rancio que yo mismo escribí",
        "Pégale al enemigo una patada en el riñón. ¿... Qué? ¿No puse esa opción?",
        "patata.",
        "¿Alguien lee esto?",
        "¿Cómo como? Como como como.",
        "Cuando termine esto voy a hibernar durante todo febrero",
        "He llegado a un punto donde cambiarle el color a algo me da pereza, ayuda",
        "¿Alguna vez te has preguntado a qué sabe un fantasma?"
    };
    
    public void elegirMensaje() {
        Random ran = new Random();
        lblConsejo.setText(mensajes[ran.nextInt(mensajes.length)]);
    }
    
    @FXML
    void cambiarPantalla(ActionEvent event) throws IOException {
        if (PeleaController.VolverAMenu == true) {
            Stage stage;
            Scene scene;
            Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
            PeleaController.VolverAMenu = false;
        } else {
            Stage stage;
            Scene scene;
            Parent root = FXMLLoader.load(getClass().getResource("Pelea.fxml"));
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnCambio.setVisible(false);
        elegirMensaje();
        delay(4000 , () -> btnCambio.fire());
    }
    
    public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }
    
}
