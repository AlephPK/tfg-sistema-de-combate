package tfjuego;

public class Objeto {
    private String nombre;
    private String descripcion;
    private int vidaBoost;
    private int atkBoost;
    private int defBoost;
    private int cantidad;

    public Objeto(String nombre, String descripcion, int vidaBoost, int atkBoost, int defBoost, int cantidad) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.vidaBoost = vidaBoost;
        this.atkBoost = atkBoost;
        this.defBoost = defBoost;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getVidaBoost() {
        return vidaBoost;
    }

    public void setVidaBoost(int vidaBoost) {
        this.vidaBoost = vidaBoost;
    }

    public int getAtkBoost() {
        return atkBoost;
    }

    public void setAtkBoost(int atkBoost) {
        this.atkBoost = atkBoost;
    }

    public int getDefBoost() {
        return defBoost;
    }

    public void setDefBoost(int defBoost) {
        this.defBoost = defBoost;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
