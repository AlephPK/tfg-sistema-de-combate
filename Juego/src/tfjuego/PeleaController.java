package tfjuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.TranslateTransition;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Paint;
import javafx.util.Duration;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;


public class PeleaController implements Initializable {
    
    @FXML
    private ImageView Cursor;
    
    @FXML
    private ImageView CursorTarget;

    @FXML
    private ImageView imgEnemigo;
    
    @FXML
    private ImageView imgAtaqueEnemigo;

    @FXML
    private ImageView imgFran;
    
    @FXML
    private ImageView imgEssie;

    @FXML
    private Label lblAtacar;

    @FXML
    private Label lblMagia;

    @FXML
    private Label lblNombreEnemigo;

    @FXML
    private Label lblObjetos;
    
    @FXML
    private ImageView barraDeAtaque;
    
    @FXML
    private Label lblListaObjetos;
    
    @FXML
    private Label lblHechizo;
    
    @FXML
    private Label lblDamageEnemigo;
    
    @FXML
    private Label lblDamageEssie;
    
    @FXML
    private Label lblDamageElver;
    
    @FXML
    private Label lblDamageEnemigoFallo;
    
    @FXML
    private ImageView imgMenu;
    
    @FXML
    private Button btnMenu;
    
    @FXML
    private Button btnCambiarDerrota;

    @FXML
    private Button btnCambiarVictoria;
    
    @FXML
    private Button btnCambiarMenu;
    
    @FXML
    private Group menuGrupo;
    
    @FXML
    private AnchorPane anchorPane;
    
    @FXML
    private ImageView imgGolpeEfecto;
    
    @FXML
    private Label lblHPElver;
    
    @FXML
    private ProgressBar prgHPElver;

    @FXML
    private ProgressBar prgHPEssie;
    
    @FXML
    private Label lblDescObjeto;
    

    @FXML
    private Label lblHPElverTotal;
    private int HPElver;
    private int HPElverTotal;

    @FXML
    private Label lblHPEssie;
    private int HPEssie;
    private int HPEssieTotal;

    @FXML
    private Label lblHPEssieTotal;
    
    @FXML
    private Group DefensaPrompt;
    
    @FXML
    private ImageView imgDefPromptX;

    @FXML
    private ImageView imgDefPromptZ;
    
    @FXML
    private ImageView imgAtaqueResultado;
    
    private boolean esTurnoJugador = false;
    
    private boolean elverDefensa = false;
    private boolean essieDefensa = false;
    
    private boolean elverKO = false;
    private boolean essieKO = false;
    
    private boolean elverMareo = false;
    private int elverMareoCuenta = 4;
    private boolean essieMareo = false;
    private int essieMareoCuenta = 4;
    
    int HPEnemigo;
    
    private Objeto nieveFrita;
    
    public static int idMusica;
    
    public static boolean VolverAMenu = false;
    
    Media mMover = new Media(new File("src/sfx/menuNav.wav").toURI().toString());
    AudioClip mover = new AudioClip(mMover.getSource());
    //

    Media mSeleccionar = new Media(new File("src/sfx/seleccionar.wav").toURI().toString());
    AudioClip seleccionar = new AudioClip(mSeleccionar.getSource());
    //

    Media mAtras = new Media(new File("src/sfx/atras.wav").toURI().toString());
    AudioClip atras = new AudioClip(mAtras.getSource());
    //

    Media mSlash = new Media(new File("src/sfx/slash.wav").toURI().toString());
    AudioClip slash = new AudioClip(mSlash.getSource());
    //
            
    Media mFallo = new Media(new File("src/sfx/fallo.wav").toURI().toString());
    AudioClip fallo = new AudioClip(mFallo.getSource());
    //

    Media mSlashCrit = new Media(new File("src/sfx/slashCritico.wav").toURI().toString());
    AudioClip slashCrit = new AudioClip(mSlashCrit.getSource());
    //

    Media mDamage = new Media(new File("src/sfx/damage.wav").toURI().toString());
    AudioClip damage = new AudioClip(mDamage.getSource());
    //
            
    Media mCurar = new Media(new File("src/sfx/curar.wav").toURI().toString());
    AudioClip curar = new AudioClip(mCurar.getSource());
            
    Media mMareoRecu = new Media(new File("src/sfx/mareoRecu.wav").toURI().toString());
    AudioClip mareoRecu = new AudioClip(mMareoRecu.getSource());
            
    Media mResBien = new Media(new File("src/sfx/resBien.wav").toURI().toString());
    AudioClip resBien = new AudioClip(mResBien.getSource());
            
    Media mResGenial = new Media(new File("src/sfx/resGenial.wav").toURI().toString());
    AudioClip resGenial = new AudioClip(mResGenial.getSource());
    
    Media mHuir = new Media(new File("src/sfx/huir.wav").toURI().toString());
    AudioClip huir = new AudioClip(mHuir.getSource());
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mover.setVolume(0.50);
        seleccionar.setVolume(0.50);
        atras.setVolume(0.50);
        slash.setVolume(0.50);
        fallo.setVolume(0.50);
        slashCrit.setVolume(0.50);
        damage.setVolume(0.75);
        
        Media msacar = new Media(new File("src/sfx/sacarEspada.wav").toURI().toString());
        AudioClip sacar = new AudioClip(msacar.getSource());
        delay(300, () -> sacar.play());
        
        DefensaPrompt.setVisible(false);
        
        imgFran.setSmooth(false);
        //Hacer la animacion de sacar la espada
        imgFran.setImage(new Image("imagenes/fran/ataque.png"));
        delay(1000, () -> imgFran.setImage(new Image("imagenes/fran/idle.png")));
        imgEssie.setImage(new Image("imagenes/essie/ataque.png"));
        delay(1000, () -> imgEssie.setImage(new Image("imagenes/essie/idle.png")));
        //Poner la musica
        delay(1000, () -> ponerCancion());
        delay(1000, () -> esTurnoJugador = true);
        
        //Ocultar lo necesario
        barraDeAtaque.setVisible(false);
        lblNombreEnemigo.setVisible(false);
        lblListaObjetos.setVisible(false);
        lblHechizo.setVisible(false);
        imgGolpeEfecto.setVisible(false);
        lblDamageEnemigo.setOpacity(0);
        lblDamageElver.setOpacity(0);
        lblDamageEssie.setOpacity(0);
        lblDamageEnemigoFallo.setOpacity(0);
        imgAtaqueResultado.setOpacity(0);
        imgAtaqueEnemigo.setVisible(false);
        mostrarMenu(false);
        delay(1000, () -> mostrarMenu(true));
        
        HPElver = 200;lblHPElver.setText(HPElver + "");prgHPElver.setProgress(HPElver/200);
        HPElverTotal = 200;lblHPElverTotal.setText("/" + HPElverTotal);
        
        HPEssie = 150;lblHPEssie.setText(HPEssie + "");prgHPEssie.setProgress(HPEssie/150);
        HPEssieTotal = 150;lblHPEssieTotal.setText("/" + HPEssieTotal);
        
        HPEnemigo = 800;
        
        btnCambiarDerrota.setVisible(false);
        btnCambiarVictoria.setVisible(false);
        btnCambiarMenu.setVisible(false);
        
        lblDescObjeto.setVisible(false);
        
        CursorTarget.setVisible(false);
        
        nieveFrita = new Objeto("Manzana", "Cura 60 HP.\nSin efectos\nespeciales", 60, 0, 0, 3);
    }
    
    public void mostrarMenu(boolean mostrar) {
        if (mostrar) {
            lblAtacar.setVisible(true);
            lblMagia.setVisible(true);
            lblObjetos.setVisible(true);
            imgMenu.setVisible(true);
            Cursor.setVisible(true);
        } else {
            lblAtacar.setVisible(false);
            lblMagia.setVisible(false);
            lblObjetos.setVisible(false);
            imgMenu.setVisible(false);
            Cursor.setVisible(false);
        }
    }
    AudioClip cancion;
    //Esto controla la canción de fondo. He descubierto que AudioClip funciona mucho mejor que MediaPlayer
    public void ponerCancion() {
        Media media = new Media(new File("src/musica/ParryAndAttack.wav").toURI().toString());
        Media media2 = new Media(new File("src/musica/comeOnAgain.wav").toURI().toString());
        switch (idMusica) {
            case 1:
                cancion = new AudioClip(media.getSource());
                cancion.setCycleCount(AudioClip.INDEFINITE);
                cancion.play();
                break;
            case 2:
                cancion = new AudioClip(media2.getSource());
                cancion.setCycleCount(AudioClip.INDEFINITE);
                cancion.play();
                break;
            default:
                cancion = new AudioClip(media.getSource());
                cancion.setCycleCount(AudioClip.INDEFINITE);
                cancion.play();
                break;
        }
    }
    
    //Esta es la función que me ha salvado la vida varias veces ya. Basicamente atrasa la ejecución de un método el tiempo que le digas
    public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }
    
    
    
    
    //Dios me salve
    int posicion = 1;
    int fuerza = 0;
    String currentPersonaje = "Elver";
    @FXML
    void navegarMenu(KeyEvent event) {
        
        if (esTurnoJugador == true) {
            
            
            if (elverMareoCuenta == 0) {
                elverMareo = false;
                if (!elverKO) {
                    mareoRecu.play();
                    if (HPElver <= (HPElverTotal * 25) / 100) {
                        imgFran.setImage(new Image("imagenes/fran/idleWeak.png"));
                    } else {
                        imgFran.setImage(new Image("imagenes/fran/idle.png"));
                    }
                }
                elverMareoCuenta = 4;
            }
            
            if (essieMareoCuenta == 0) {
                essieMareo = false;
                mareoRecu.play();
                if (!essieKO) {
                    if (HPEssie <= (HPEssieTotal * 25) / 100) {
                        imgEssie.setImage(new Image("imagenes/essie/idleWeak.png"));
                    } else {
                        imgEssie.setImage(new Image("imagenes/essie/idle.png"));
                    }
                }
                essieMareoCuenta = 4;
            }

            //Problema: se activa dos veces y no se por qué
            //Solución: .consume();
            if (event.getCode().equals(KeyCode.DOWN) && posicion < 3) { //Cuando va hacia abajo
                event.consume();
                mover.play();
                posicion++;
                switch (posicion) {
                    case 2:
                        Cursor.setLayoutY(116);
                        break;
                    case 3:
                        Cursor.setLayoutY(190);
                        break;
                }
            }
            if (event.getCode().equals(KeyCode.UP) && posicion > 1 && posicion <= 3) { //Cuando va hacia arriba
                event.consume();
                mover.play();
                posicion--;
                switch (posicion) {
                    case 2:
                        Cursor.setLayoutY(116);
                        break;
                    case 1:
                        Cursor.setLayoutY(47);
                        break;
                }
            }

            //COSAS DEL ATAQUE
            //opcion de atacar
            if (event.getCode().equals(KeyCode.Z) && posicion == 1) {
                if (currentPersonaje.equals("Elver")) {
                    event.consume();
                    seleccionar.play();
                    lblNombreEnemigo.setVisible(true);
                    Cursor.setLayoutX(160);
                    imgFran.setImage(new Image("imagenes/fran/ataquePrep.png"));
                    delay(100, () -> posicion = 11);
                } else if (currentPersonaje.equals("Essie")) {
                    event.consume();
                    seleccionar.play();
                    lblNombreEnemigo.setVisible(true);
                    Cursor.setLayoutX(160);
                    imgEssie.setImage(new Image("imagenes/essie/ataquePrep.png"));
                    delay(100, () -> posicion = 11);
                }
            }

            //confirmar ataque
            if (currentPersonaje.equals("Elver")) {
                if (event.getCode().equals(KeyCode.Z) && posicion == 11) {
                    event.consume();
                    posicion = 12;
                    Cursor.setVisible(false);
                    seleccionar.play();
                    barraDeAtaque.setVisible(true);
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueFran.gif"));

                    delay(20, () -> fuerza = 1);
                    delay(750, () -> fuerza = 2);
                    delay(900, () -> fuerza = 3);
                    delay(1000, () -> fuerza = 0);
                    delay(1200, () -> barraDeAtaque.setVisible(false));
                    if (HPElver <= (HPElverTotal * 25) / 100) {
                        delay(2000, () -> imgFran.setImage(new Image("imagenes/fran/idleWeak.png")));
                    } else {
                        delay(2000, () -> imgFran.setImage(new Image("imagenes/fran/idle.png")));
                    }
                    
                    
                    if(!essieKO && !essieMareo){
                        delay(2500, () -> currentPersonaje = "Essie");
                        delay(2500, () -> posicion = 1);
                        delay(2500, () -> Cursor.setLayoutY(47));
                        delay(2500, () -> Cursor.setLayoutX(14));
                        delay(2500, () -> Cursor.setVisible(true));
                        delay(2000, () -> cambiarMenu(2, true));
                    }
                    
                    else{
                        delay(2000, () -> cambiarMenu(1, false));
                        delay(2000, () -> esTurnoJugador=false);
                        delay(2500, () -> currentPersonaje = "Elver");
                        delay(2500, () -> posicion = 1);
                        delay(2500, () -> Cursor.setLayoutY(47));
                        delay(2500, () -> Cursor.setLayoutX(14));
                        delay(2500, () -> Cursor.setVisible(true));
                        delay(3000,()->ataqueEnemigo());
                        if (essieMareo) {
                            essieMareoCuenta--;
                        }
                    }

                }

                if (event.getCode().equals(KeyCode.Z) && fuerza == 1) {
                    event.consume();
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueFranFallo.gif"));
                    imgFran.setImage(new Image("imagenes/fran/ataqueFallo.png"));
                    fallo.play();
                    ataqueResultadoFade(0,700);
                    fuerza = 0;
                } else if (event.getCode().equals(KeyCode.Z) && fuerza == 2) {
                    event.consume();
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueFranAmarillo.gif"));
                    imgFran.setImage(new Image("imagenes/fran/ataque.png"));
                    slash.play();
                    resBien.play();
                    imgGolpeEfecto.setImage(new Image("imagenes/efectos/franSlash.gif"));
                    imgGolpeEfecto.setVisible(true);
                    delay(500, () -> damage.play());
                    delay(500, () -> enemigoDamageFade(700));
                    delay(500, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/auch.png")));
                    delay(750, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/idle.png")));
                    HPEnemigo = HPEnemigo - 30;
                    lblDamageEnemigo.setText("30");
                    ataqueResultadoFade(1,700);
                    fuerza = 0;
                } else if (event.getCode().equals(KeyCode.Z) && fuerza == 3) {
                    event.consume();
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueFranRojo.gif"));
                    imgFran.setImage(new Image("imagenes/fran/ataque.png"));
                    slashCrit.play();
                    resGenial.play();
                    imgGolpeEfecto.setImage(new Image("imagenes/efectos/franSlash.gif"));
                    imgGolpeEfecto.setVisible(true);
                    HPEnemigo = HPEnemigo - 50;
                    lblDamageEnemigo.setText("50");
                    ataqueResultadoFade(2,700);
                    delay(500, () -> damage.play());
                    delay(500, () -> enemigoDamageFade(700));
                    delay(500, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/auch.png")));
                    delay(750, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/idle.png")));
                    fuerza = 0;
                }
            } else if (currentPersonaje.equals("Essie")) {
                if (event.getCode().equals(KeyCode.Z) && posicion == 11) {
                    event.consume();
                    posicion = 12;
                    Cursor.setVisible(false);
                    seleccionar.play();
                    barraDeAtaque.setVisible(true);
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueEssie.gif"));

                    delay(20, () -> fuerza = 1);
                    delay(750, () -> fuerza = 2);
                    delay(900, () -> fuerza = 3);
                    delay(1000, () -> fuerza = 0);
                    delay(1200, () -> barraDeAtaque.setVisible(false));

                    delay(2000, () -> cambiarMenu(1, false));
                    delay(2000, () -> esTurnoJugador=false);
                    if (HPEssie <= (HPEssieTotal * 25) / 100) {
                        delay(2000, () -> imgEssie.setImage(new Image("imagenes/essie/idleWeak.png")));
                    } else {
                        delay(2000, () -> imgEssie.setImage(new Image("imagenes/essie/idle.png")));
                    }
                    delay(2500, () -> currentPersonaje = "Elver");
                    delay(2500, () -> posicion = 1);
                    delay(2500, () -> Cursor.setLayoutY(47));
                    delay(2500, () -> Cursor.setLayoutX(14));
                    delay(2500, () -> Cursor.setVisible(true));
                    delay(3000,()->ataqueEnemigo());

                }

                if (event.getCode().equals(KeyCode.Z) && fuerza == 1) {
                    event.consume();
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueEssieFallo.gif"));
                    imgEssie.setImage(new Image("imagenes/essie/ataqueFallo.png"));
                    fallo.play();
                    ataqueResultadoFade(0,700);
                    fuerza = 0;
                } else if (event.getCode().equals(KeyCode.Z) && fuerza == 2) {
                    event.consume();
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueEssieAmarillo.gif"));
                    imgEssie.setImage(new Image("imagenes/essie/ataque.png"));
                    slash.play();
                    resBien.play();
                    ataqueResultadoFade(1,700);
                    imgGolpeEfecto.setImage(new Image("imagenes/efectos/essieSlash.gif"));
                    imgGolpeEfecto.setVisible(true);
                    HPEnemigo = HPEnemigo - 40;
                    lblDamageEnemigo.setText("40");
                    delay(500, () -> damage.play());
                    delay(500, () -> enemigoDamageFade(700));
                    delay(500, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/auch.png")));
                    delay(750, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/idle.png")));
                    fuerza = 0;
                } else if (event.getCode().equals(KeyCode.Z) && fuerza == 3) {
                    event.consume();
                    barraDeAtaque.setImage(new Image("imagenes/barraAtaque/ataqueEssieRojo.gif"));
                    imgEssie.setImage(new Image("imagenes/essie/ataque.png"));
                    slashCrit.play();
                    resGenial.play();
                    ataqueResultadoFade(2,700);
                    imgGolpeEfecto.setImage(new Image("imagenes/efectos/essieSlash.gif"));
                    imgGolpeEfecto.setVisible(true);
                    HPEnemigo = HPEnemigo - 60;
                    lblDamageEnemigo.setText("60");
                    delay(500, () -> damage.play());
                    delay(500, () -> enemigoDamageFade(700));
                    delay(500, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/auch.png")));
                    delay(750, () -> imgEnemigo.setImage(new Image("imagenes/enemigoFinal/idle.png")));
                    fuerza = 0;
                }
            }

            //Volver
            if (event.getCode().equals(KeyCode.X) && posicion == 11) {
                event.consume();
                atras.play();
                lblNombreEnemigo.setVisible(false);
                Cursor.setLayoutX(14);
                if (!elverKO && !elverMareo) {
                    if (HPElver <= (HPElverTotal * 25) / 100) {
                        imgFran.setImage(new Image("imagenes/fran/idleWeak.png"));
                    } else {
                        imgFran.setImage(new Image("imagenes/fran/idle.png"));
                    }
                }
                if (!essieKO && !essieMareo) {
                    if (HPEssie <= (HPEssieTotal * 25) / 100) {
                        imgEssie.setImage(new Image("imagenes/essie/idleWeak.png"));
                    } else {
                        imgEssie.setImage(new Image("imagenes/essie/idle.png"));
                    }
                }
                delay(100, () -> posicion = 1);
            }

            //COSAS DE OBJETOS
            if (event.getCode().equals(KeyCode.Z) && posicion == 2) {
                if (currentPersonaje.equals("Elver")) {
                    event.consume();
                    seleccionar.play();
                    lblListaObjetos.setText(nieveFrita.getNombre() + " x" + nieveFrita.getCantidad());
                    lblListaObjetos.setVisible(true);
                    lblDescObjeto.setText(nieveFrita.getDescripcion());
                    lblDescObjeto.setVisible(true);
                    Cursor.setLayoutY(47);
                    Cursor.setLayoutX(160);
                    imgFran.setImage(new Image("imagenes/fran/objetoPrep.png"));
                    delay(100, () -> posicion = 21);
                } else if (currentPersonaje.equals("Essie")) {
                    event.consume();
                    seleccionar.play();
                    lblListaObjetos.setText(nieveFrita.getNombre() + " x" + nieveFrita.getCantidad());
                    lblListaObjetos.setVisible(true);
                    lblDescObjeto.setText(nieveFrita.getDescripcion());
                    lblDescObjeto.setVisible(true);
                    Cursor.setLayoutY(47);
                    Cursor.setLayoutX(160);
                    imgEssie.setImage(new Image("imagenes/essie/objetoPrep.png"));
                    delay(100, () -> posicion = 21);
                }
            }
            
            //Elegir objeto
            if (event.getCode().equals(KeyCode.Z) && posicion == 21) {
                Media mProhibido = new Media(new File("src/sfx/prohibido.wav").toURI().toString());
                AudioClip prohibido = new AudioClip(mProhibido.getSource());
                if(nieveFrita.getCantidad() > 0){
                    if (currentPersonaje.equals("Elver")) {
                        seleccionar.play();
                        event.consume();
                        Cursor.setVisible(false);
                        CursorTarget.setVisible(true);
                        delay(100, () -> posicion = 211);
                    } else if (currentPersonaje.equals("Essie")) {
                        seleccionar.play();
                        event.consume();
                        Cursor.setVisible(false);
                        CursorTarget.setVisible(true);
                        delay(100, () -> posicion = 211);
                    }
                } else{
                    prohibido.play();
                }
            }
            
            //Elegir a quien dar el objeto
            if (event.getCode().equals(KeyCode.DOWN) && posicion == 211) { //Elegir a Essie
                event.consume();
                mover.play();
                //CursorTarget.setLayoutX(369);
                //CursorTarget.setLayoutY(322);
                CursorTarget.setLayoutX(233);
                CursorTarget.setLayoutY(561);
                delay(100, () -> posicion = 212);
            }
            
            if (event.getCode().equals(KeyCode.UP) && posicion == 212) { //Elegir a Elver
                event.consume();
                mover.play();
                CursorTarget.setLayoutX(369);
                CursorTarget.setLayoutY(322);
                //CursorTarget.setLayoutX(233);
                //CursorTarget.setLayoutY(561);
                delay(100, () -> posicion = 211);
            }
            
            if (event.getCode().equals(KeyCode.Z) && posicion == 211) { //usar el objeto (REESCRIBE ESTO)
                event.consume();
                seleccionar.play();
                CursorTarget.setVisible(false);
                if (currentPersonaje.equals("Elver")){
                    delay(200, () -> {
                        curar.play();
                        if (HPElver + nieveFrita.getVidaBoost() >= HPElverTotal) {
                            HPElver = HPElverTotal;
                            lblHPElver.setText(HPElver + "");
                            prgHPElver.setProgress(HPElver / 200);
                        } else {
                            HPElver = HPElver + nieveFrita.getVidaBoost();
                            lblHPElver.setText(HPElver + "");
                            prgHPElver.setProgress(HPElver / 200);
                        }
                    });
                    imgFran.setImage(new Image("imagenes/fran/objeto.png"));
                    delay(500 , () -> imgFran.setImage(new Image("imagenes/fran/idle.png")));
                    lblDescObjeto.setVisible(false);
                    if(!essieKO && !essieMareo){
                        delay(1000, () -> currentPersonaje = "Essie");
                        delay(1000, () -> posicion = 1);
                        delay(1000, () -> Cursor.setLayoutY(47));
                        delay(1000, () -> Cursor.setLayoutX(14));
                        delay(1000, () -> Cursor.setVisible(true));
                        delay(500, () -> cambiarMenu(2, true));
                    }
                    
                    else{
                        delay(500, () -> cambiarMenu(1, false));
                        delay(500, () -> esTurnoJugador=false);
                        delay(1000, () -> currentPersonaje = "Elver");
                        delay(1000, () -> posicion = 1);
                        delay(1000, () -> Cursor.setLayoutY(47));
                        delay(1000, () -> Cursor.setLayoutX(14));
                        delay(1000, () -> Cursor.setVisible(true));
                        delay(1500,()->ataqueEnemigo());
                    }
                } else if (currentPersonaje.equals("Essie")){
                    delay(200, () -> {
                        curar.play();
                        if (HPElver + nieveFrita.getVidaBoost() >= HPElverTotal) {
                            HPElver = HPElverTotal;
                            lblHPElver.setText(HPElver + "");
                            prgHPElver.setProgress(HPElver / 200);
                        } else {
                            HPElver = HPElver + nieveFrita.getVidaBoost();
                            lblHPElver.setText(HPElver + "");
                            prgHPElver.setProgress(HPElver / 200);
                        }
                    });
                    imgEssie.setImage(new Image("imagenes/essie/objeto.png"));
                    delay(500 , () -> imgEssie.setImage(new Image("imagenes/essie/idle.png")));
                    lblDescObjeto.setVisible(false);
                    delay(1000, () -> cambiarMenu(1, false));
                    delay(1000, () -> esTurnoJugador=false);
                    delay(1500, () -> currentPersonaje = "Elver");
                    delay(1500, () -> posicion = 1);
                    delay(1500, () -> Cursor.setLayoutY(47));
                    delay(1500, () -> Cursor.setLayoutX(14));
                    delay(1500, () -> Cursor.setVisible(true));
                    delay(2000,()->ataqueEnemigo());
                }
                nieveFrita.setCantidad(nieveFrita.getCantidad() - 1);
            }
            
            if (event.getCode().equals(KeyCode.Z) && posicion == 212) {
                event.consume();
                seleccionar.play();
                CursorTarget.setVisible(false);
                if (currentPersonaje.equals("Elver")){
                    delay(200, () -> {
                        curar.play();
                        if (HPEssie + nieveFrita.getVidaBoost() >= HPEssieTotal) {
                            HPEssie = HPEssieTotal;
                            lblHPEssie.setText(HPEssie + "");
                            prgHPEssie.setProgress(HPEssie / HPEssieTotal);
                        } else {
                            HPEssie = HPEssie + nieveFrita.getVidaBoost();
                            lblHPEssie.setText(HPEssie + "");
                            prgHPEssie.setProgress(HPEssie / HPEssieTotal);
                        }
                    });
                    imgFran.setImage(new Image("imagenes/fran/objeto.png"));
                    delay(500 , () -> imgFran.setImage(new Image("imagenes/fran/idle.png")));
                    lblDescObjeto.setVisible(false);
                    if(!essieKO && !essieMareo){
                        delay(1000, () -> currentPersonaje = "Essie");
                        delay(1000, () -> posicion = 1);
                        delay(1000, () -> Cursor.setLayoutY(47));
                        delay(1000, () -> Cursor.setLayoutX(14));
                        delay(1000, () -> Cursor.setVisible(true));
                        delay(500, () -> cambiarMenu(2, true));
                    }
                    
                    else{
                        delay(500, () -> cambiarMenu(1, false));
                        delay(500, () -> esTurnoJugador=false);
                        delay(1000, () -> currentPersonaje = "Elver");
                        delay(1000, () -> posicion = 1);
                        delay(1000, () -> Cursor.setLayoutY(47));
                        delay(1000, () -> Cursor.setLayoutX(14));
                        delay(1000, () -> Cursor.setVisible(true));
                        delay(1500,()->ataqueEnemigo());
                    }
                } else if (currentPersonaje.equals("Essie")){
                    delay(200, () -> {
                        curar.play();
                        if (HPEssie + nieveFrita.getVidaBoost() >= HPEssieTotal) {
                            HPEssie = HPEssieTotal;
                            lblHPEssie.setText(HPEssie + "");
                            prgHPEssie.setProgress(HPEssie / HPEssieTotal);
                        } else {
                            HPEssie = HPEssie + nieveFrita.getVidaBoost();
                            lblHPEssie.setText(HPEssie + "");
                            prgHPEssie.setProgress(HPEssie / 200);
                        }
                    });
                    imgEssie.setImage(new Image("imagenes/essie/objeto.png"));
                    delay(500 , () -> imgEssie.setImage(new Image("imagenes/essie/idle.png")));
                    lblDescObjeto.setVisible(false);
                    delay(1000, () -> cambiarMenu(1, false));
                    delay(1000, () -> esTurnoJugador=false);
                    delay(1500, () -> currentPersonaje = "Elver");
                    delay(1500, () -> posicion = 1);
                    delay(1500, () -> Cursor.setLayoutY(47));
                    delay(1500, () -> Cursor.setLayoutX(14));
                    delay(1500, () -> Cursor.setVisible(true));
                    delay(2000,()->ataqueEnemigo());
                }
                nieveFrita.setCantidad(nieveFrita.getCantidad() - 1);
            }

            //atras
            if (event.getCode().equals(KeyCode.X) && (posicion == 21 || posicion == 211)) {
                event.consume();
                atras.play();
                lblListaObjetos.setVisible(false);
                lblDescObjeto.setVisible(false);
                Cursor.setLayoutY(116);
                Cursor.setLayoutX(14);
                Cursor.setVisible(true);
                CursorTarget.setVisible(false);
                lblDescObjeto.setVisible(false);
                if (!elverKO && !elverMareo){
                    if (HPElver <= (HPElverTotal * 25) / 100) {
                        imgFran.setImage(new Image("imagenes/fran/idleWeak.png"));
                    } else {
                        imgFran.setImage(new Image("imagenes/fran/idle.png"));
                    }
                }
                if (!essieKO && !essieMareo){
                    if (HPEssie <= (HPEssieTotal * 25) / 100) {
                        imgEssie.setImage(new Image("imagenes/essie/idleWeak.png"));
                    } else {
                        imgEssie.setImage(new Image("imagenes/essie/idle.png"));
                    }
                }
                delay(100, () -> posicion = 2);
            }

            //ORIGINALMENTE COSAS DE MAGIA, AHORA FUNCION DE HUIR
            if (event.getCode().equals(KeyCode.Z) && posicion == 3) {
                event.consume();
                seleccionar.play();
                lblHechizo.setText("Huir");
                lblHechizo.setVisible(true);
                Cursor.setLayoutY(47);
                Cursor.setLayoutX(160);
                delay(100, () -> posicion = 31);
            }
            //cancelar
            if (event.getCode().equals(KeyCode.X) && posicion == 31) {
                event.consume();
                atras.play();
                lblHechizo.setVisible(false);
                Cursor.setLayoutY(190);
                Cursor.setLayoutX(14);
                delay(100, () -> posicion = 3);
            }
            //confirmar
            if (event.getCode().equals(KeyCode.Z) && posicion == 31) {
                posicion = 311;
                menuGrupo.setVisible(false);
                seleccionar.play();
                
                //PONER CONDICIONALES PARA EL KO LUEGO
                if (elverKO) {
                    imgFran.setVisible(false);
                    imgEssie.setImage(new Image("imagenes/essie/huirCarga.png"));
                    imgEssie.setScaleX(-1);
                    imgEssie.setLayoutY(250);
                    saltar("essie");
                    delay(500, () -> moverPersonaje("essie"));
                } else if (essieKO){
                    imgEssie.setVisible(false);
                    imgFran.setImage(new Image("imagenes/fran/huirCarga.png"));
                    imgFran.setScaleX(-1);
                    imgFran.setLayoutY(250);
                    saltar("fran");
                    delay(500, () -> moverPersonaje("fran"));
                }else{
                    imgFran.setImage(new Image("imagenes/fran/huir.png"));
                    imgFran.setScaleX(-1);
                    saltar("fran");
                    delay(500, () -> moverPersonaje("fran"));

                    imgEssie.setImage(new Image("imagenes/essie/huir.png"));
                    imgEssie.setScaleX(-1);
                    saltar("essie");
                    delay(500, () -> moverPersonaje("essie"));

                    
                }
                delay(500, () -> huir.play());
                delay(1000, () -> btnCambiarMenu.fire());
                delay(1000, () -> cancion.stop());
            }
        } else{ //DEFENSA (Elver)
            if ((event.getCode().equals(KeyCode.Z)) && essieDefensa==false){
                if(!elverKO && !elverMareo) {
                    if (elverDefensa == false) {
                        event.consume();
                        imgFran.setImage(new Image("imagenes/fran/defender.png"));
                        elverDefensa = true;
                    } /*else {
                        event.consume();
                        imgFran.setImage(new Image("imagenes/fran/defender.png"));
                    }*/
                }
            }
            
            if ((event.getCode().equals(KeyCode.X)) && elverDefensa==false){ //Essie
                if(!essieKO && !essieMareo) {
                    if (essieDefensa == false) {
                        event.consume();
                        imgEssie.setImage(new Image("imagenes/essie/defender.png"));
                        essieDefensa = true;
                    } /*else {
                        event.consume();
                        imgEssie.setImage(new Image("imagenes/essie/defender.png"));
                    }*/
                }
            }
        }
    }
    
    void moverPersonaje(String pers) {
        if (pers.equalsIgnoreCase("fran")) {
            TranslateTransition translate = new TranslateTransition();
            translate.setNode(imgFran);
            translate.setDuration(Duration.millis(250));
            //translate.setByY(181);
            translate.setByX(-800);
            translate.play();
        } else if (pers.equalsIgnoreCase("essie")) {
            TranslateTransition translate = new TranslateTransition();
            translate.setNode(imgEssie);
            translate.setDuration(Duration.millis(250));
            //translate.setByY(336);
            translate.setByX(-800);
            translate.play();
        }
    }
    
    void saltar(String pers) {
        if (pers.equalsIgnoreCase("fran")) {
            TranslateTransition translation = new TranslateTransition(Duration.millis(250));
            translation.setNode(imgFran);
            translation.interpolatorProperty().set(Interpolator.SPLINE(.1, .1, .7, .7));
            translation.setByY(-200);
            translation.setAutoReverse(true);
            translation.setCycleCount(2);
            translation.play();
        } else if (pers.equalsIgnoreCase("essie")) {
            TranslateTransition translation = new TranslateTransition(Duration.millis(250));
            translation.setNode(imgEssie);
            translation.interpolatorProperty().set(Interpolator.SPLINE(.1, .1, .7, .7));
            translation.setByY(-200);
            translation.setAutoReverse(true);
            translation.setCycleCount(2);
            translation.play();
        }
    }
    
    void mostrarDefensaPrompt (boolean mostrar) {
        if (elverKO || elverMareo) {
            imgDefPromptZ.setVisible(false);
        } else {
            imgDefPromptZ.setVisible(true);
        }
        
        if (essieKO || essieMareo) {
            imgDefPromptX.setVisible(false);
        } else {
            imgDefPromptX.setVisible(true);
        }
        if (mostrar) {
            DefensaPrompt.setVisible(true);
            FadeTransition fadeTransition = new FadeTransition();
            fadeTransition.setDuration(Duration.millis(250));
            fadeTransition.setNode(DefensaPrompt);
            fadeTransition.setFromValue(0);
            fadeTransition.setToValue(1);
            fadeTransition.play();
        } else {
            FadeTransition fadeTransition = new FadeTransition();
            fadeTransition.setDuration(Duration.millis(250));
            fadeTransition.setNode(DefensaPrompt);
            fadeTransition.setFromValue(1);
            fadeTransition.setToValue(0);
            fadeTransition.play();
        }
    }
    @FXML
    void soltarDefensa(KeyEvent event) {
        if (esTurnoJugador == false) {
            elverDefensa = false;
            essieDefensa = false;
            
            if (!elverKO && !elverMareo){
                if (HPElver <= (HPElverTotal*25)/100) {
                    imgFran.setImage(new Image("imagenes/fran/idleWeak.png"));
                } else {
                    imgFran.setImage(new Image("imagenes/fran/idle.png"));
                }
                
            }
            if (!essieKO && !essieMareo) {
                if (HPEssie <= (HPEssieTotal * 25) / 100) {
                    imgEssie.setImage(new Image("imagenes/essie/idleWeak.png"));
                } else {
                    imgEssie.setImage(new Image("imagenes/essie/idle.png"));
                }
            }
        }
    }
    
    private void enemigoDamageFade(double duracion) {
        FadeTransition fadeTransition = new FadeTransition();
        fadeTransition.setDuration(Duration.millis(duracion));
        fadeTransition.setNode(lblDamageEnemigo);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.play();
    }
    
    private void DamageFade(double duracion, String personaje) {
        FadeTransition fadeTransition = new FadeTransition();
        fadeTransition.setDuration(Duration.millis(duracion));
        if(personaje.equals("Elver")){
            fadeTransition.setNode(lblDamageElver);
        } else{
            fadeTransition.setNode(lblDamageEssie);
        }
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.play();
    }
    
    /*private void enemigoDamageFalloFade(double duracion) {
        imgAtaqueResultado.setImage(new Image("imagenes/misc/ataqueRes/fallo.png"));
        FadeTransition fadeTransition = new FadeTransition();
        fadeTransition.setDuration(Duration.millis(duracion));
        fadeTransition.setNode(imgAtaqueResultado);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.play();
    }*/
    
    private void ataqueResultadoFade(int res, double duracion) {
        switch (res) {
            case 1:imgAtaqueResultado.setImage(new Image("imagenes/misc/ataqueRes/bien.png"));/*System.out.println("res: " + res)*/;break;
            case 2:imgAtaqueResultado.setImage(new Image("imagenes/misc/ataqueRes/genial.png"));/*System.out.println("res: " + res)*/;break;
            default:imgAtaqueResultado.setImage(new Image("imagenes/misc/ataqueRes/fallo.png"));/*System.out.println("res: " + res)*/;break;
        }
        delay(10, () -> {
            FadeTransition fadeTransition = new FadeTransition();
            fadeTransition.setDuration(Duration.millis(duracion));
            fadeTransition.setNode(imgAtaqueResultado);
            fadeTransition.setFromValue(1);
            fadeTransition.setToValue(0);
            fadeTransition.play();
        });
    }
    
    public void cambiarMenu(int personaje, boolean volver) {
        FadeTransition fadeTransition = new FadeTransition();
        fadeTransition.setDuration(Duration.millis(250));
        fadeTransition.setNode(menuGrupo);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        fadeTransition.play();
        
        FadeTransition fadeTransition2 = new FadeTransition();
        fadeTransition2.setDuration(Duration.millis(250));
        fadeTransition2.setNode(menuGrupo);
        fadeTransition2.setFromValue(0);
        fadeTransition2.setToValue(1);
        if(volver== true) {
            delay(260, () -> fadeTransition2.play());
        }
        delay(250, () -> lblNombreEnemigo.setVisible(false));
        delay(250, () -> lblListaObjetos.setVisible(false));
        delay(250, () -> lblHechizo.setVisible(false));
        if (personaje == 1) {
            delay(250, () -> imgMenu.setImage(new Image("imagenes/menuFran.png")));
        } else {
            delay(250, () -> imgMenu.setImage(new Image("imagenes/menuEssie.png")));
        }
    }
    
    public void devolverMenu(int personaje) {
        
        
        FadeTransition fadeTransition2 = new FadeTransition();
        fadeTransition2.setDuration(Duration.millis(250));
        fadeTransition2.setNode(menuGrupo);
        fadeTransition2.setFromValue(0);
        fadeTransition2.setToValue(1);
        fadeTransition2.play();
        delay(250, () -> lblNombreEnemigo.setVisible(false));
        delay(250, () -> lblListaObjetos.setVisible(false));
        delay(250, () -> lblHechizo.setVisible(false));
        if (personaje == 1) {
            imgMenu.setImage(new Image("imagenes/menuFran.png"));
        } else {
            imgMenu.setImage(new Image("imagenes/menuEssie.png"));
        }
    }
    
    public void moverAtaqueEnemigo(int posX, int posY, long duracion) {
        TranslateTransition translate = new TranslateTransition();
        translate.setNode(imgAtaqueEnemigo);
        translate.setDuration(Duration.millis(duracion));
        translate.setByX(posX);
        translate.setByY(posY);
        translate.play();
    }
    
    public void ataqueEnemigo() {
        if(HPEnemigo > 0) {
            mostrarDefensaPrompt(true);
            int target;
            if (!elverKO && !essieKO) {
                imgAtaqueEnemigo.setVisible(true);
                Random random = new Random();
                target = random.nextInt(2);
            } else if (elverKO) {
                imgAtaqueEnemigo.setVisible(true);
                target = 1;
            } else{
                imgAtaqueEnemigo.setVisible(true);
                target = 0;
            }
            switch (target) {
                case 0:
                    moverAtaqueEnemigo(-570, -150, 1000);
                    delay(1000, () -> moverAtaqueEnemigo(570, 150, 1000));
                    delay(1000, () -> comprobarGolpeEnemigo(true));
                    delay(1000, () -> imgAtaqueEnemigo.setVisible(false));
                    delay(1000, () -> mostrarDefensaPrompt(false));
                    break;
                case 1:
                    moverAtaqueEnemigo(-680, 85, 1000);
                    delay(1000, () -> moverAtaqueEnemigo(680, -85, 1000));
                    delay(1000, () -> comprobarGolpeEnemigo(false));
                    delay(1000, () -> imgAtaqueEnemigo.setVisible(false));
                    delay(1000, () -> mostrarDefensaPrompt(false));
                    break;
            }
        }else {
            matarEnemigo();
            delay(1000, () -> btnCambiarVictoria.fire());
        }
        
    }
    
    public void comprobarGolpeEnemigo(boolean target) {
        Media mDamage = new Media(new File("src/sfx/damage.wav").toURI().toString());
        AudioClip damage = new AudioClip(mDamage.getSource());
        
        Media mMareo = new Media(new File("src/sfx/mareo.wav").toURI().toString());
        AudioClip mareo = new AudioClip(mMareo.getSource());
        
        Media mBell = new Media(new File("src/sfx/bell.wav").toURI().toString());
        AudioClip bell = new AudioClip(mBell.getSource());
        
        Media mKo = new Media(new File("src/sfx/ko.wav").toURI().toString());
        AudioClip ko = new AudioClip(mKo.getSource());
        
        Random random = new Random();
        if(target == true) {
            if (elverDefensa == false) {
                HPElver = HPElver - 30;
                lblHPElver.setText(HPElver + "");
                lblDamageElver.setText("30");
                prgHPElver.setProgress(prgHPElver.getProgress() - 0.15);
                
                int rMareo = random.nextInt(4);
                //int rMareo = 1;
                
                if (HPElver > 0 && (rMareo != 1 || elverMareo)) { //No se marea
                    imgFran.setImage(new Image("imagenes/fran/auch.png"));
                    if (elverMareo) {
                        delay(500, () -> imgFran.setImage(new Image("imagenes/fran/mareo.png")));
                    } else if (HPElver <= (HPElverTotal * 25) / 100) {
                        delay(500, () -> imgFran.setImage(new Image("imagenes/fran/idleWeak.png")));
                    } else {
                        delay(500, () -> imgFran.setImage(new Image("imagenes/fran/idle.png")));
                    }
                    damage.play();
                    DamageFade(700, "Elver");
                    
                } else if (HPElver > 0 && rMareo == 1 && !essieKO && !elverMareo && !essieMareo){ //Se marea
                    imgFran.setImage(new Image("imagenes/fran/mareo.png"));
                    mareo.play();
                    DamageFade(700, "Elver");
                    elverMareo = true;
                } else{ //Se queda KO
                    imgFran.setImage(new Image("imagenes/fran/auch.png"));
                    delay(500, () -> imgFran.setImage(new Image("imagenes/fran/ko.png")));
                    damage.play();
                    delay(500, () -> ko.play());
                    DamageFade(700, "Elver");
                    elverKO = true;
                }
                if (elverKO || elverMareo) {
                    if (!essieKO){
                        if (elverMareo) {
                            elverMareoCuenta--;
                        }
                        delay(1000, () -> devolverMenu(2));
                        delay(1000, () -> esTurnoJugador = true);
                        currentPersonaje = "Essie";
                    } else {
                        cancion.stop();
                        delay(1000, () -> btnCambiarDerrota.fire());
                    }
                } else {
                    delay(1000, () -> devolverMenu(1));
                    delay(1000, () -> esTurnoJugador = true);
                }
            } else{
                bell.play();
                if (elverKO || elverMareo) {
                    if (elverMareo) {
                        elverMareoCuenta--;
                    }
                    delay(1000, () -> devolverMenu(2));
                    delay(1000, () -> esTurnoJugador = true);
                    currentPersonaje = "Essie";
                } else {
                    delay(1000, () -> devolverMenu(1));
                    delay(1000, () -> esTurnoJugador = true);
                }
            }
        } else {
            if (essieDefensa == false) {
                HPEssie = HPEssie - 30;
                lblHPEssie.setText(HPEssie + "");
                lblDamageEssie.setText("30");
                prgHPEssie.setProgress(prgHPEssie.getProgress() - 0.2);
                
                int rMareo = random.nextInt(4);
                //int rMareo = 1;
                
                if (HPEssie > 0 && (rMareo != 1 || essieMareo)) {
                    imgEssie.setImage(new Image("imagenes/essie/auch.png"));
                    if (essieMareo) {
                        delay(500, () -> imgEssie.setImage(new Image("imagenes/essie/mareo.png")));
                    } else if (HPEssie <= (HPEssieTotal * 25) / 100) {
                        delay(500, () -> imgEssie.setImage(new Image("imagenes/essie/idleWeak.png")));
                    } else {
                        delay(500, () -> imgEssie.setImage(new Image("imagenes/essie/idle.png")));
                    }
                    damage.play();
                    DamageFade(700, "Essie");
                } else if (HPEssie > 0 && rMareo == 1 && !elverKO && !essieMareo && !elverMareo){
                    imgEssie.setImage(new Image("imagenes/essie/mareo.png"));
                    mareo.play();
                    DamageFade(700, "Essie");
                    essieMareo = true;
                }else{
                    imgEssie.setImage(new Image("imagenes/essie/auch.png"));
                    delay(500, () -> imgEssie.setImage(new Image("imagenes/essie/ko.png")));
                    damage.play();
                    delay(500, () -> ko.play());
                    DamageFade(700, "Essie");
                    essieKO = true;
                }
                if (elverKO || elverMareo) {
                    if(!essieKO) {
                        if (elverMareo) {
                            elverMareoCuenta--;
                        }
                        delay(1000, () -> devolverMenu(2));
                        delay(1000, () -> esTurnoJugador = true);
                        currentPersonaje = "Essie";
                    } else{
                        cancion.stop();
                        delay(1000, () -> btnCambiarDerrota.fire());
                    }
                } else {
                    delay(1000, () -> devolverMenu(1));
                    delay(1000, () -> esTurnoJugador = true);
                }
            } else{ //no le ha dado
                bell.play();
                if (elverKO || elverMareo) {
                    delay(1000, () -> devolverMenu(2));
                    delay(1000, () -> esTurnoJugador = true);
                    currentPersonaje = "Essie";
                    if (elverMareo) {
                        elverMareoCuenta--;
                    }
                } else {
                    delay(1000, () -> devolverMenu(1));
                    delay(1000, () -> esTurnoJugador = true);
                }
            }
            
        }
    }
    
    public void matarEnemigo() {
        Media mKo = new Media(new File("src/sfx/ko.wav").toURI().toString());
        AudioClip ko = new AudioClip(mKo.getSource());
        ko.play();
        cancion.stop();
        imgEnemigo.setImage(new Image("imagenes/enemigoFinal/ko.png"));
    }
    
    @FXML
    void cambiarDerrota(ActionEvent event) throws IOException {
        VolverAMenu = true;
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Derrota.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void cambiarVictoria(ActionEvent event) throws IOException {
        VolverAMenu = true;
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Victoria.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
    void cambiarMenu(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
