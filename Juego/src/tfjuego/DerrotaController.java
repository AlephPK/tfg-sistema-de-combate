/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package tfjuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Alguien
 */
public class DerrotaController implements Initializable {

    @FXML
    private Button btnVolver;
    
    @FXML
    private Button control;

    @FXML
    void control(KeyEvent event) {
        if (event.getCode().equals(KeyCode.Z)){
            btnVolver.fire();
        }
    }

    @FXML
    void volverMenu(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Carga.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        derr.stop();
    }
    
    Media mDerr = new Media(new File("src/musica/derrota.wav").toURI().toString());
    AudioClip derr = new AudioClip(mDerr.getSource());
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnVolver.setVisible(false);
        derr.play();
    }    
    
}
