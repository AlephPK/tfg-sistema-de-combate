/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package tfjuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Alguien
 */
public class MusicaMenuController implements Initializable {

    @FXML
    private Button control;

    @FXML
    private Label lblMus;
    
    @FXML
    private ImageView imgTitulo;
    
    @FXML
    private Button btnVolver;

    int posicion = 1;
    @FXML
    void navegarMenu(KeyEvent event) {
        if (event.getCode().equals(KeyCode.RIGHT) || event.getCode().equals(KeyCode.LEFT)){
            switch (posicion) {
                case 1:
                    event.consume();
                    posicion = 2;
                    ataca.stop();
                    noPerd.setCycleCount(AudioClip.INDEFINITE);
                    noPerd.play();
                    lblMus.setText("¡No podemos perder!");
                    imgTitulo.setImage(new Image("imagenes/misc/musTitulos/2.png"));
                    break;
                case 2:
                    event.consume();
                    posicion = 1;
                    noPerd.stop();
                    ataca.setCycleCount(AudioClip.INDEFINITE);
                    ataca.play();
                    lblMus.setText("Ataca al enemigo");
                    imgTitulo.setImage(new Image("imagenes/misc/musTitulos/1.png"));
                    break;
            }
        }
        
        if (event.getCode().equals(KeyCode.Z)) {
            ataca.stop();
            noPerd.stop();
            switch (posicion) {
                case 1:
                    PeleaController.idMusica = 1;
                    btnVolver.fire();
                    break;
                case 2:
                    PeleaController.idMusica = 2;
                    btnVolver.fire();
                    break;
            }
            
        }
    }
    
    @FXML
    void volverMenu(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    Media mAtaca = new Media(new File("src/musica/ParryAndAttack.wav").toURI().toString());
    AudioClip ataca = new AudioClip(mAtaca.getSource());
    
    Media mNoPerd = new Media(new File("src/musica/comeOnAgain.wav").toURI().toString());
    AudioClip noPerd = new AudioClip(mNoPerd.getSource());
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblMus.setAlignment(Pos.CENTER);
        lblMus.setVisible(false);
        lblMus.setText("Ataca al enemigo");
        ataca.setCycleCount(AudioClip.INDEFINITE);
        ataca.play();
        btnVolver.setVisible(false);
    }
}
