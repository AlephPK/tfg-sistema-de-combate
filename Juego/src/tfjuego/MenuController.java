package tfjuego;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.stage.Stage;

public class MenuController implements Initializable{

    @FXML
    private Button btnJugar;
    
    @FXML
    private Button btnGuia;
    
    @FXML
    private Button btnMusica;
    
    @FXML
    private Button btnCreditos;
    
    @FXML
    private ImageView imgBotones;
    
    int posicion = 1;
    
    Media mMover = new Media(new File("src/sfx/menuNav.wav").toURI().toString());
    AudioClip mover = new AudioClip(mMover.getSource());
    
    Media mSeleccionar = new Media(new File("src/sfx/seleccionar.wav").toURI().toString());
    AudioClip seleccionar = new AudioClip(mSeleccionar.getSource());
    
    Media mProhibido = new Media(new File("src/sfx/prohibido.wav").toURI().toString());
    AudioClip prohibido = new AudioClip(mProhibido.getSource());
    
    Media mCancion = new Media(new File("src/musica/MindTheSunstrokes.wav").toURI().toString());
    AudioClip cancion = new AudioClip(mCancion.getSource());
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnJugar.setVisible(false);
        btnGuia.setVisible(false);
        btnMusica.setVisible(false);
        btnCreditos.setVisible(false);
        mover.setVolume(0.50);
        seleccionar.setVolume(0.50);
        cancion.setCycleCount(AudioClip.INDEFINITE);
        cancion.play();
        PeleaController.VolverAMenu = false;
    }

    @FXML
    void lanzarJuego(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Carga.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
    void lanzarGuia(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Guia.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
    void lanzarMusica(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("MusicaMenu.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
    void lanzarCreditos(ActionEvent event) throws IOException {
        Stage stage;
        Scene scene;
        Parent root = FXMLLoader.load(getClass().getResource("Creditos.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
    void navegarMenu(KeyEvent event) {
        //MOVERSE
        if (event.getCode().equals(KeyCode.RIGHT)){
            switch (posicion) {
                case 1: //seleccionar guia
                    posicion = 2;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/2.png"));
                    mover.play();
                    event.consume();
                    break;
                case 2: //seleccionar musica
                    posicion = 3;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/3.png"));
                    mover.play();
                    event.consume();
                    break;
                case 3: //seleccionar creditos
                    posicion = 4;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/4.png"));
                    mover.play();
                    event.consume();
                    break;
                default: //seleccionar jugar
                    posicion = 1;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/1.png"));
                    mover.play();
                    event.consume();
                    break;
                
            }
        }
        if (event.getCode().equals(KeyCode.LEFT)){
            switch (posicion) {
                case 4: //seleccionar musica
                    posicion = 3;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/3.png"));
                    mover.play();
                    event.consume();
                    break;
                case 3: //seleccionar guia
                    posicion = 2;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/2.png"));
                    mover.play();
                    event.consume();
                    break;
                case 2: //seleccionar jugar
                    posicion = 1;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/1.png"));
                    mover.play();
                    event.consume();
                    break;
                default: //seleccionar creditos
                    posicion = 4;
                    imgBotones.setImage(new Image("imagenes/misc/btnMenu/4.png"));
                    mover.play();
                    event.consume();
                    break;
                
            }
        }
        //SELECCIONAR
        if(event.getCode().equals(KeyCode.Z)) {
            switch(posicion) {
                case 1: //JUGAR
                    seleccionar.play();
                    cancion.stop();
                    btnJugar.fire();
                    event.consume();
                    break;
                case 2: //GUIA
                    seleccionar.play();
                    cancion.stop();
                    btnGuia.fire();
                    event.consume();
                    break;
                case 3: //MUSICA
                    seleccionar.play();
                    cancion.stop();
                    btnMusica.fire();
                    event.consume();
                    break;
                default: //CREDITOS
                    seleccionar.play();
                    cancion.stop();
                    btnCreditos.fire();
                    event.consume();
                    break;
            }
        }
    }

}

